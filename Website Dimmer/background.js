// SETTINGS
var prefsDefaults = {
   "nightMode": false,
   "fullDim": true,
   "dimMore": false,
   "smoothFades": false,
   "textColor": 'slate gray',
   "dimLevel": 30,
   "manualDimValue": 70
};
var prefsColorSets = {
  "wheat": [245,222,179],
  "burlywood": [222,184,135],
  "quartz": [217,217,243],
  "gray": [150,150,150],
  "slate gray": [112,128,144],
  "orange": [180, 80, 1],
  "copper": [184,115,51],
  "tomato": [255,99,71],
  "feldspar": [209,146,117],
  "sandy brown": [244,164,96],
  "sienna": [142,107,35],
  "golden rod": [218,165,32],
  "yellow": [205,205,0],
  "dark yellow": [150,150,50],
  "khaki": [139,134,78],
  "olive": [128,128,0],
  "green": [60, 150, 1],
  "green yellow": [173,255,47],
  "lawn green": [124,252,0],
  "chartreuse": [102,205,0],
  "aquamarine": [112,219,147],
  "sea green": [35,142,104],
  "blue": [10, 70, 190],
  "cornflower blue": [100,149,237],
  "irish blue": [3,180,200],
  "cyan": [0,255,255],
  "light cyan": [209,238,238],
  "medium orchid": [147,112,219],
  "violet": [238,130,238],
  "purple": [160,32,240],
  "thistle": [216,191,216],
  "peach puff": [255,218,185],
  "black": [1,1,1],
  "white": [254,254,254]
};

// STORE DEFAULT PREFERENCES IN STORAGE IF THERE ARE NONE
if (typeof localStorage['prefsDomainsDefaults'] === 'undefined') {
   localStorage['prefsDomainsDefaults'] = JSON.stringify(prefsDefaults);
}

// READ SITE SPECIFIC PREFS OR DEFAULT IF NOT PRESENT AND
// RETURN JSON OBJECT
function readPrefsFromStorage(domain) {
   try {
      var prefs = JSON.parse(localStorage["prefsDomain_" + domain]);
      if (typeof prefs['nightMode'] !== 'undefined') {
        prefs.textColorSets = prefsColorSets;
        prefsisSettingForCurrentWebsiteStored = true;
         return prefs;
      } else {
        prefs = JSON.parse(localStorage['prefsDomainsDefaults']);
        prefs.textColorSets = prefsColorSets;
        prefsisSettingForCurrentWebsiteStored = false;
        return prefs;
      }
   } catch (error) {
      try {
        prefs = JSON.parse(localStorage['prefsDomainsDefaults']);
        prefs.textColorSets = prefsColorSets;
        prefsisSettingForCurrentWebsiteStored = false;
        return prefs;
      } catch (e) {
         return undefined;
      }
   }
}

function readPrefsFromStorageDefault(prefsName) {
   return JSON.parse(localStorage[prefsName]);
}


function storePrefsForCurrentWebsite(menu) {
  chrome.tabs.query({active: true, currentWindow: true}, function (tab) {
    var domain, prefs;

    domain = returnDomainName(tab[0].url);
    prefs = readPrefsFromStorage(domain);

    //Set dimLevel
    if ( dimLevelPrefix(menu.menuItemId) === 'dimLevel_' ) {
      prefs['dimLevel'] = returnDimLevelMenuValue(menu.menuItemId);
    }

    //Set textColor
    if ( dimLevelPrefix(menu.menuItemId) === 'textColor_' ) {
      prefs['textColor'] = menu.menuItemId.split('_')[1];
    }

    switch (menu.menuItemId) {
      case 'nightMode': prefs['nightMode'] = menu.checked; break;
      case 'smoothFades': prefs['smoothFades'] = menu.checked; break;
      case 'dimMore': prefs['dimMore'] = menu.checked; break;
      case 'fullDim_true': prefs['fullDim'] = true; break;
      case 'fullDim_false': prefs['fullDim'] = false; break;
    }

    localStorage['prefsDomain_'+domain] = JSON.stringify(prefs);

    scriptSettingsUpdate(tab, prefs);
    if(menu.menuItemId === 'nightMode') {
      turnOnNightMode(tab[0], prefs);
    }
  });
}


function storePrefsDefault() {
  chrome.tabs.query({active: true, currentWindow: true}, function (tab) {
    var domain, prefsDefaults;

    domain = returnDomainName(tab[0].url);
    prefsDefaults = readPrefsFromStorage(domain);
    localStorage['prefsDomainsDefaults'] = JSON.stringify(prefsDefaults);
  });
}


function deleteCurrentDomainPrefs () {
  chrome.tabs.query({active: true, currentWindow: true}, function (tab) {
    if (allowedAddresses(tab[0].url)) {
      var domain = returnDomainName(tab[0].url);
      localStorage.removeItem("prefsDomain_" + domain);
      chrome.browserAction.setBadgeText({text: '', tabId: tab[0].id});
    }
  });
}

function returnDomainName(myString) {
 return myString.split('/')[2].split('').map(function(e){return e.charCodeAt(0) }).join('');
 //return myString.split('/')[2];
}

function dimLevelPrefix(itemId) {
   return itemId.substring(0, itemId.indexOf("_")+1);
}

function returnDimLevelMenuValue(itemId) {
   return parseInt( itemId.substring(itemId.indexOf("_") + 1)  );
}


function getPrefs (property) {
   return prefsDefaults[property];
}

function scriptSettingsUpdate(tab, prefs) {
         if (allowedAddresses(tab.url)) {
            chrome.tabs.sendMessage(tab.id, {command: "updatePrefs", 'prefs': prefs}, function(){});
         }
}
// END OF SETTINGS


//MESSAGE LISTENER
chrome.runtime.onMessage.addListener(function(request, sender, response){
   if (request.command = "getPrefs") {
      response ( readPrefsFromStorage(returnDomainName(sender.url)) );
   }
});

// CREATE MENUS
chrome.contextMenus.create({
   title: 'Reset Colors (Alt+Shift+R)',
   contexts: ['all'],
   type: 'normal',
   onclick: resetStyles
});

function resetStyles() {
   chrome.tabs.query({active: true, currentWindow: true}, function (tabs) {
      chrome.tabs.sendMessage(tabs[0].id, {command: 'reset', prefs: readPrefsFromStorage(returnDomainName(tabs[0].url))}, function () {});
   });
}

chrome.contextMenus.create({
   title: 'Night Mode',
   contexts: ['browser_action'],
   type: "checkbox",
   id: "nightMode",
   checked: getPrefs('nightMode') || false,
   onclick: storePrefsForCurrentWebsite
});

function turnOnNightMode(tab, prefs) {
      if (allowedAddresses(tab.url) ) {
         chrome.tabs.sendMessage(tab.id, {command: "nightMode", prefs: prefs}, function(){});
      }
}

chrome.contextMenus.create({
   title: 'Dim Levels',
   contexts: ['browser_action'],
   type: "normal",
   id: "dimLevelsParent"
});

chrome.contextMenus.create({
   title: 'Dim to Full Black',
   contexts: ['browser_action'],
   type: "radio",
   id: "fullDim_true",
   parentId: "dimLevelsParent",
   checked: getPrefs('fullDim') === true,
   onclick: storePrefsForCurrentWebsite
});

chrome.contextMenus.create({
   title: 'or pick Dim Level:',
   contexts: ['browser_action'],
   type: "radio",
   id: "fullDim_false",
   parentId: "dimLevelsParent",
   checked: getPrefs('fullDim') === false,
   onclick: storePrefsForCurrentWebsite
});


chrome.contextMenus.create({
   type:'separator',
   contexts: ['browser_action'],
   parentId: "dimLevelsParent"
});

for (var i = 5; i<101; i+=5) {
   chrome.contextMenus.create({
      title: 'Dim level '+i+'%',
      contexts: ['browser_action'],
      type: "radio",
      id: "dimLevel_"+i,
      parentId: "dimLevelsParent",
      checked: getPrefs('dimLevel') == i,
      onclick: storePrefsForCurrentWebsite
   });
}

chrome.contextMenus.create({
   title: 'Text Colors',
   contexts: ['browser_action'],
   type: "normal",
   id: "textColorsParent"
});

for (color in prefsColorSets) {
  chrome.contextMenus.create({
    title: color,
    contexts: ['browser_action'],
    type: "radio",
    id: "textColor_" + color,
    parentId: "textColorsParent",
    checked: ( getPrefs('textColor') === color ),
    onclick: storePrefsForCurrentWebsite
  });
}

chrome.contextMenus.create({
  title: 'Other Options',
  contexts: ['browser_action'],
  type: "normal",
  id: "otherOptions"
});

chrome.contextMenus.create({
  parentId: 'otherOptions',
  title: 'Dim More (can dim to much)',
  contexts: ['browser_action'],
  type: "checkbox",
  id: "dimMore",
  checked: getPrefs('prefsDimMore'),
  onclick: storePrefsForCurrentWebsite
});

chrome.contextMenus.create({
  parentId: 'otherOptions',
  title: 'Smooth Fades',
  contexts: ['browser_action'],
  type: "checkbox",
  id: "smoothFades",
  checked: getPrefs('prefsSmoothFades') || false,
  onclick: storePrefsForCurrentWebsite
});

chrome.contextMenus.create({
  parentId: 'otherOptions',
  type:'separator',
  contexts: ['browser_action']
});

chrome.contextMenus.create({
  parentId: 'otherOptions',
  title: 'Store current settings as default',
  contexts: ['browser_action'],
  type: "normal",
  id: "storeDefaultSettings",
  onclick: storePrefsDefault
});

chrome.contextMenus.create({
  parentId: 'otherOptions',
  type:'separator',
  contexts: ['browser_action']
});

chrome.contextMenus.create({
  parentId: 'otherOptions',
  title: 'Clear current website settings',
  contexts: ['browser_action'],
  type: "normal",
  id: "deleteSettings",
  onclick: deleteCurrentDomainPrefs
});

//LISTENERS
chrome.tabs.onActivated.addListener(function(){
   updateMenu();
});

chrome.tabs.onUpdated.addListener(function(tabId, tabInfo){
   if (tabInfo.status === 'loading') {
      updateMenu();
   }
});


function updateMenu() {
   chrome.tabs.query({'active': true, 'currentWindow': true}, function (tabs) {

      if (allowedAddresses(tabs[0].url)) {
         var domain = returnDomainName(tabs[0].url);
         var prefs = readPrefsFromStorage(domain);
         chrome.contextMenus.update('nightMode', {'checked': prefs.nightMode});
         chrome.contextMenus.update('smoothFades', {'checked': prefs.smoothFades});
         chrome.contextMenus.update('dimMore', {'checked': prefs.dimMore});

         chrome.contextMenus.update('fullDim_true', {'checked': prefs.fullDim === true});
         chrome.contextMenus.update('fullDim_false', {'checked': prefs.fullDim === false});

         for (var i = 5; i < 101; i += 5) {
            if (prefs.dimLevel === i) {
               chrome.contextMenus.update('dimLevel_' + i, {'checked': true});
            } else {
               chrome.contextMenus.update('dimLevel_' + i, {'checked': false});
            }
         }
         for (color in prefsColorSets) {
           chrome.contextMenus.update('textColor_' + color, {'checked': prefs.textColor === color});
         }

        if (prefsisSettingForCurrentWebsiteStored && tabs[0].id !== null) {
           chrome.browserAction.setBadgeText({text: "s", tabId: tabs[0].id});
         }
      }
   });
}
//SHORTCUT KEY COMMANDS
chrome.commands.onCommand.addListener(function (command) {
   if (command === 'Website Dimmer - Dim') {
      chrome.tabs.query({active: true, currentWindow: true}, function (tabs) {
         chrome.tabs.sendMessage(tabs[0].id, {command: "dimRequest"}, function () {});
      });
   }
   if (command === 'Website Dimmer - Reset') {
      chrome.tabs.query({active: true, currentWindow: true}, function (tabs) {
         chrome.tabs.sendMessage(tabs[0].id, {command: "reset", prefs: readPrefsFromStorage(returnDomainName(tabs[0].url))}, function () {});
      });
   }
});

//BUTTON CLICK
chrome.browserAction.onClicked.addListener(function (tabs) {
   if (allowedAddresses(tabs.url)){
      chrome.tabs.sendMessage(tabs.id, {command: "dimRequest"}, function (response) {
      });
   }
});

//UTILITY FUNCTIONS
function allowedAddresses(p) {
   if (typeof p !== 'undefined') {
      return p.substr(0, 4) === "http" || p.substr(0, 4) === "file";
   }
}