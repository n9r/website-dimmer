//console.log("WDScript1.js");
var WDObserverPreDOM, WDObserverPostDOM, WDSettings;

chrome.runtime.sendMessage(null, {command: "getPrefs"}, function (response) {
   WDSettings = response;
   init();
});

function init() {
   if (typeof WDInitialized === 'undefined') {

     if (typeof WDObserverPreDOM === "undefined" ) { WDObserverStartup(); }

      // RECEIVE MESSAGES FROM CONTEXT MENU
      chrome.runtime.onMessage.addListener( function (request) {
            if (request.command === 'reset') {
               WDSettings = request.prefs;
               resetStyles();
            } else if (request.command === 'dimRequest') {
               manualDim();
            } else if (request.command === 'nightMode') {
               WDSettings = request.prefs;
               if (WDSettings.nightMode === true) {
                  nightModeOperation();
                  if (typeof WDObserverPostDOM === 'undefined') {
                     stopObserver(WDObserverPreDOM);
                     WDObserverPostDOM = setupObserver(handleMutationsPostDOM);
                  }
               } else {
                  resetStyles();
                  if (typeof WDObserverPostDOM !== 'undefined') {
                     stopObserver(WDObserverPostDOM);
                  }
               }
            } else if (request.command === 'recolorizeLetters') {
               recolorizeLetters();
            } else if (request.command === 'updatePrefs') {
               WDSettings = request.prefs;
            }
         }
      );
      WDInitialized = true;
   } // INITIALIZE END
}

function WDObserverStartup() {
   if (typeof WDObserverPreDOM === "undefined" && WDSettings.nightMode) {

     WDObserverPreDOM = setupObserver(handleMutationsPreDOM);

     document.addEventListener("DOMContentLoaded", function () {
         WDObserverPreDOM.disconnect();
         WDObserverPostDOM = setupObserver(handleMutationsPostDOM);
        });
   }
}


function nightModeOperation() {

  var documentBody = document.body;
  if (documentBody === null || document === null) { return;}

    darkenHtmlAndBody(document.querySelector('HTML:not([wdmodified])'));
    darkenHtmlAndBody(document.querySelector('BODY:not([wdmodified])'));


  var elementsToModify = documentBody.querySelectorAll(':not([wdmodified]):not(SCRIPT):not(STYLE):not(IMG):not(FIGURE):not(VIDEO):not(FONT)');

   if (elementsToModify !== null) {
      for (var el in elementsToModify) {
         if (elementsToModify.hasOwnProperty(el)) {
            darkenElementInNightMode(elementsToModify[el]);
         }
      }
   }
}

function darkenElementInNightMode(element) {
   if (element === null) {
      return;
   }

   var bgColorsArray, bgColorsArrayMod;

   bgColorsArray = returnComputedStyleToArray(element, isDimMore() );

   if (WDSettings.fullDim) {
      //do we have RGBA or RGB
      if (typeof bgColorsArray[3] !== 'undefined') {
         element.style.setProperty(isDimMore(), "rgba(0,0,0,"+ bgColorsArray[3]
            +")", "important");
      } else {
         element.style.setProperty(isDimMore(), "rgb(0,0,0)", "important");
      }
   } else {
      bgColorsArrayMod = dimLevelAlgo(bgColorsArray);
      if (typeof bgColorsArray[3] !== 'undefined') {
         element.style.setProperty(isDimMore()
            , "rgba(" +
            bgColorsArrayMod[0] + "," +
            bgColorsArrayMod[1] + "," +
            bgColorsArrayMod[2] + ","
            + bgColorsArray[3] + ")", "important");
      } else {
         element.style.setProperty(isDimMore()
            , "rgb(" +
            bgColorsArrayMod[0] + "," +
            bgColorsArrayMod[1] + "," +
            bgColorsArrayMod[2] + ")", "important");
      }
   }
   colorizeLetters(element);
}


function darkenHtmlAndBody(element) {

   if (element === null || element.hasAttribute('wdmodified')) {
      return;
   }

   var bgColorsArray;
   setTransitions(element);

   bgColorsArray = returnComputedStyleToArray(element, isDimMore());

   if (WDSettings.fullDim) {
      //do we have RGBA or RGB
      if (typeof bgColorsArray[3] !== 'undefined') {
         element.style.setProperty(isDimMore(), "rgba(0,0,0,1)", "important");
      } else {
         element.style.setProperty(isDimMore(), "rgb(0,0,0)", "important");
      }
   } else {
      bgColorsArrayMod = dimLevelAlgo(bgColorsArray);
      if (typeof bgColorsArray[3] !== 'undefined') {
         element.style.setProperty(isDimMore()
            , "rgba(" +
            bgColorsArrayMod[0] + "," +
            bgColorsArrayMod[1] + "," +
            bgColorsArrayMod[2] + ","
            + 1 + ")", "important");
      } else {
         element.style.setProperty(isDimMore()
            , "rgb(" +
            bgColorsArrayMod[0] + "," +
            bgColorsArrayMod[1] + "," +
            bgColorsArrayMod[2] + ")", "important");
      }
   }
   colorizeLetters(element);
}


function dimLevelAlgo (array) {
   return array.map(function(el,table,index){

      if (index !== 3) {
         return parseInt( (el - ( 255 * Math.log( 1 + el / 255 ) ) ) / (   WDSettings.dimLevel / 15 ) );
         //return parseInt( el - ( el * 0.85 ) ) ;
      } else {
         return el;
      }
   });
}


// MANUAL DIM
function manualDim() {

   var elementsToModify;

   darkenHtmlAndBody(document.querySelector('HTML:not([wdmodified])'), WDSettings.manualDimValue);
   darkenHtmlAndBody(document.querySelector('BODY:not([wdmodified])'), WDSettings.manualDimValue);

   elementsToModify = document.body.querySelectorAll(':not(SCRIPT):not(IMG):not(FIGURE):not(VIDEO):not(STYLE)');

   if (elementsToModify === null) {
      return;
   }

   for (var el in elementsToModify) {
      if (elementsToModify.hasOwnProperty(el)) {
         darkenElementManualDim(elementsToModify[el]);
      }
   }
}

function darkenElementManualDim(element) {
  var bgColorsArray;

  if (WDSettings.smoothFades && !element.style.transition) {
    element.style.setProperty("transition", "background-color 1.2s");
  } else if (!WDSettings.smoothFades && element.style.transition) {
    element.style.removeProperty("transition");
  }

  bgColorsArray = returnComputedStyleToArray(
    element, isDimMore()
  );

  //dimMore OPTION
  if ( WDSettings.dimMore === true
    && ( bgColorsArray[3] === 0 || typeof bgColorsArray[3] === 'undefined' )
    && ( element.tagName !== "BODY"  ) ) {
    bgColorsArray[0] = 20;
    bgColorsArray[1] = 60;
    bgColorsArray[2] = 80;
    bgColorsArray[3] = 1;
  }


  // DECREASING VALUES UNTIL 0
  if (bgColorsArray[0] > 0) {
    bgColorsArray[0] -= WDSettings.manualDimValue;
  } else {
    bgColorsArray[0] = 0;
  }
  if (bgColorsArray[1] > 0) {
    bgColorsArray[1] -= WDSettings.manualDimValue;
  } else {
    bgColorsArray[1] = 0;
  }
  if (bgColorsArray[2] > 0) {
    bgColorsArray[2] -= WDSettings.manualDimValue;
  } else {
    bgColorsArray[2] = 0;
  }

  //do we have RGBA or RGB
  if (typeof bgColorsArray[3] !== 'undefined' && (
    bgColorsArray[0] != 0
    || bgColorsArray[1] != 0
    || bgColorsArray[2] != 0 )) {

    element.style.setProperty( isDimMore()
      , "rgba("
      + bgColorsArray[0] + ","
      + bgColorsArray[1] + ","
      + bgColorsArray[2] + ","
      + bgColorsArray[3] + ")"
      , "important");
  } else if (typeof bgColorsArray[3] === 'undefined'
    && (  bgColorsArray[0] != 0
    || bgColorsArray[1] != 0
    || bgColorsArray[2] != 0 )) {

    element.style.setProperty( isDimMore()
      , "rgb("
      + bgColorsArray[0] + ","
      + bgColorsArray[1] + ","
      + bgColorsArray[2] + ")"
      , "important");

  } else if (
    bgColorsArray[0] == 0
    || bgColorsArray[1] == 0
    || bgColorsArray[2] == 0) {

    if (bgColorsArray[0] === 0 && bgColorsArray[1] === 0 && bgColorsArray[2] === 0) {
    }

  }
  colorizeLetters(element);
}
// END OF MANUAL DIM


// RETURNS COMPUTED STYLE AS ARRAY OF NUMBERS
function returnComputedStyleToArray(element, property) {

   var computedBgColor = window
      .getComputedStyle(element, null)
      .getPropertyValue(property);

   return computedBgColor
      .substring(computedBgColor.indexOf("(") + 1, computedBgColor.indexOf(")"))
      .split(",")
      .map(Number);
}


//SET COLOR PARAMETER TO DEFINED VALUE (MOSTLY FOR TEXT)
function colorizeLetters(el) {

   if ( !el.hasAttribute('wdmodified') ) {

      var bgComputedArray, colComputedArray;

      bgComputedArray = returnComputedStyleToArray(el, isDimMore() );
      colComputedArray = returnComputedStyleToArray(el, "color");

      if (el.tagName === "INPUT" || el.tagName === "TEXTAREA" || el.tagName === "BUTTON" ) {
         if (bgComputedArray[3] === undefined) {
            el.style.setProperty("background", "RGB(20,20,20)", "important");
         } else {
            el.style.setProperty("background", "RGB(20,20,20," + bgComputedArray[3] + ")", "important");
         }
         el.style.setProperty("color", "RGB(200,200,200)", "important");
      } else {
         var rgbResult = [];

         rgbResult[0] = rgbClamp( WDSettings.textColorSets[WDSettings.textColor][0] + parseInt(colComputedArray[0] / 4) );
         rgbResult[1] = rgbClamp( WDSettings.textColorSets[WDSettings.textColor][1] + parseInt(colComputedArray[1] / 4) );
         rgbResult[2] = rgbClamp( WDSettings.textColorSets[WDSettings.textColor][2] + parseInt(colComputedArray[2] / 4) );

         el.style.setProperty("color", "RGB(" + rgbResult[0] + "," + rgbResult[1] + "," + rgbResult[2] + ")", "important");
      }
      el.setAttribute("wdmodified", "");
   }
}


function recolorizeLetters() {
   var elementsToModify = document.body.querySelectorAll('[wdmodified]');

   if (elementsToModify === null) { return; }

   for (var el in elementsToModify) {
      if (elementsToModify.hasOwnProperty(el)) {
         recolorize(elementsToModify[el]);
      }
   }
}

function recolorize(el) {
  colComputedArray = returnComputedStyleToArray(el, "color");

  switch (WDSettings.textColor) {
    case 'green':
      el.style.setProperty("color", "RGB("
        + (colComputedArray[0] * 3)
        + ","
        + (colComputedArray[2] - 30)
        + ","
        + (colComputedArray[1] + 30)
        + ")", "important");
      break;
    case 'blue':
      el.style.setProperty("color", "RGB("
        + (colComputedArray[0] / 3)
        + ","
        + (colComputedArray[2] - 30)
        + ","
        + (colComputedArray[1] + 30)
        + ")", "important");
      break;
  }
}

// RESET STYLES
function resetStyles() {
   var elementsToModify = document.querySelectorAll(":not(STYLE):not(HEAD):not(IMG):not(FIGURE):not(SCRIPT)");

   for (var el in elementsToModify) {
      if (elementsToModify.hasOwnProperty(el)) {
         propertiesRemoval(elementsToModify[el]);
      }
   }
   WDObserverPostDOM.disconnect();

   propertiesRemoval(document.querySelector('HTML:not([wdmodified])'));
   propertiesRemoval(document.querySelector('BODY:not([wdmodified])'));

   if (WDSettings.nightMode === true) {
      WDSettings.nightMode = false;
   }
}

function setTransitions (element) {
   if (WDSettings.smoothFades && !element.style.transition) {
      element.style.setProperty("transition", "background-color 1.2s, color 2s");
   }
}

function propertiesRemoval(e) {
  if (WDSettings.smoothFades) {
    e.style.setProperty("transition", "background-color 2s");
  } else {
    e.style.removeProperty("transition");
  }
  e.style.removeProperty("background");
  e.style.removeProperty("background-color");
  e.style.removeProperty("color");
  e.removeAttribute("wdmodified");
  //e.style.removeProperty("transition");
}
//RESET STYLES END


// MUTATION OBSERVERS
function setupObserver(mutationHandler) {
   var observer = new MutationObserver(mutationHandler);
   observer.observe(document.documentElement, {
      attributes: false,
      childList: true,
      characterData: false,
      subtree: true
   });
   return observer;
}

function handleMutationsPreDOM(mutation) {
   mutation.forEach(function (e) {
     var eAdded = e.target;
     var tagName = eAdded.tagName;

     if ( !eAdded.hasAttribute('wdmodified') ) {
          if (tagName === 'BODY' || tagName === 'HTML'){
            darkenHtmlAndBody(eAdded);
         } else if (eAdded.nodeType === 1
            && tagName !== 'HEAD'
            && tagName !== 'STYLE'
            && tagName !== 'IMG'
            && tagName !== 'FIGURE'
            && tagName !== 'SCRIPT' ) {
            darkenElementInNightMode(eAdded);
            nightModeOperation();
         }
      }
   });
}

function handleMutationsPostDOM() {


   if (WDSettings.nightMode === true) {
      nightModeOperation();
   }
}

function stopObserver(observer) {
   if (typeof observer !== "undefined") {
      observer.disconnect();
   }
}
// END OF MUTATION OBSERVER SETUP


//utility functions
function isDimMore () {
   return WDSettings.dimMore ? 'background' : 'background-color';
}

function rgbClamp(number){
  return Math.max(0, ( Math.min( number, 255 ) ) );
}
